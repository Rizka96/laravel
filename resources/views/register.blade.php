<html>
<head>
    <title>form</title>
</head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            {{ csrf_field() }}
            <p>First Name:</p>
            <input type="text" name=fname><br>
            
            <p>Last Name:</p>
            <input type="text" name=lname><br>
            
            <p>Gender:</p>
            <input type="radio" name="gender" value="Laki-laki">Male <br>
            <input type="radio" name="gender" value="Perempuan">Female <br>
            <input type="radio" name="gender" value="other">Other <br>
            
            <p>Nationality:</p>
            <select name="nationality" id="">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Inggris">Inggris</option>
                <option value="Australia">Australia</option>
            </select>
            
            <p>Language Spoken:</p>
            <input type="checkbox" name="bahasa" value="bhs-indo">Bahasa Indonesia
            <br><input type="checkbox" name="bahasa" value="english">English
            <br><input type="checkbox" name="bahasa" value="other">Other <br>
            
            <p>Bio:</p>
            <textarea name="bio" id="biodata" cols="30" rows="10"></textarea><br><br>
        
            <button type="submit">Submit</button>
        </form>
    </body>
</html>